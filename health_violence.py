# -*- coding: utf-8 -*-
from trytond.model import ModelView,ModelSQL, fields


__all__ = ['RegistroViolencia', 'TipoViolencia', 'Denuncia']


class RegistroViolencia(ModelSQL,ModelView):
    'Programa de Registro de Situaciones de Violencia'
    __name__ = 'violencia.registro.situaciones'

    paciente = fields.Many2One('gnuhealth.patient','Paciente', 
                               required=True)    

    dni = fields.Function(fields.Char('DNI'),'get_dni')

    def get_dni(self, paciente):
        if self.paciente:
           return self.paciente.name.ref
        return None
        
    edad = fields.Function(fields.Char('Edad'),'get_paciente_age')

    def get_paciente_age(self, paciente):
        if self.paciente:
            return self.paciente.age
        return None

    domicilio = fields.Function(fields.Char('Domicilio'),'get_domicilio')

    def get_domicilio(self, name):
        res = ''
        if self.paciente:
            if self.paciente.name.addresses:
                res = self.paciente.name.addresses[0].street
        return res

    fecha_hora = fields.DateTime(u'Fecha de Registro', required=True)

    numero = fields.Integer('Num Ficha', required=True)

    violencia = fields.Many2One('violencia.tipos','Tipo de Violencia', required=True)

    observacion = fields.Text('Observaciones')    

    cant_hijos = fields.Integer('Cantidad Hijos')

    pareja = fields.Boolean('Pareja')

    hijos = fields.Boolean('Hijos')

    padre = fields.Boolean('Padre')

    madre = fields.Boolean('Madre')

    hermanos = fields.Boolean('Hermanos')

    otros = fields.Boolean('Otros')

    otros_texto = fields.Char('Otros')

    denuncia_previa = fields.Selection([
        (None, ''),
        ('SI', 'SI'),
        ('NO', 'NO'),
        ], u'Existe denuncia?', sort=False)

    campo_denuncia = fields.One2Many('violencia.denuncia','registro','Registro de Denuncias')

    familiar = fields.Many2One('gnuhealth.patient', u'Familiar/Acompaniante')

    domicilio_fam = fields.Function(fields.Char(u'Domicilio Fam/Acomp'),'get_domicilio_fam')

    def get_domicilio_fam(self, name):
        res = ''
        if self.familiar:
            if self.familiar.name.addresses:
                res = self.familiar.name.addresses[0].street
        return res

    telefono = fields.Char('Telefono')

    contacto = fields.Char('Medio de Contacto')

    profesional = fields.Many2One('gnuhealth.healthprofessional','Profesional que Registra', required=True)

    ninos_riesgo = fields.Selection([
        (None, ''),
        ('SI', 'SI'),
        ('NO', 'NO'),
        ], u'Hay niños en Riesgo?', sort=False)

    riesgo = fields.Selection([
        (None, ''),
        ('SI', 'SI'),
        ('NO', 'NO'),
        ], u'La Persona esta en Riesgo?', sort=False)

    consulta = fields.Selection([
        (None, ''),
        ('SI', 'SI'),
        ('NO', 'NO'),
        ], u'Realizo consulta previa?', sort=False)

    institucion = fields.Many2One('gnuhealth.institution', u'Institución')

    ayuda = fields.Char('Ayuda Recibida')

    atencion = fields.Char('Atendido por')

    atencion_previa = fields.Selection([
        (None, ''),
        ('SI', 'SI'),
        ('NO', 'NO'),
        ], u'Recibio atencion previa en el CAPS?', sort=False)

    motivo = fields.Char('Motivo')

    atencion_por = fields.Many2One('gnuhealth.healthprofessional', 'Profesional')

    fecha_atencion_previa = fields.Date(u'Fecha de la atención previa')

    tipo_ayuda = fields.Text('Tipo de ayuda que necesita')


class Denuncia(ModelSQL,ModelView):
    'Registro de denuncias'
    __name__ = 'violencia.denuncia'

    fecha = fields.Date('Fecha Denuncia')

    expediente = fields.Char('Nro expediente')

    denunciante = fields.Many2One('gnuhealth.patient', 'Denunciante')
   
    registro = fields.Many2One('violencia.registro.situaciones','Registro')


class TipoViolencia(ModelSQL,ModelView):
    'Registro de los tipos de violencia'

    __name__ = 'violencia.tipos'

    name = fields.Char('Tipo de Violencia', required=True)

    codigo = fields.Char('Codigo')

    descripcion = fields.Text('Descripcion')