# -*- coding: utf-8 -*-

from trytond.pool import Pool
from .health_violence import *
from .report import *


def register():
    Pool.register(        
        TipoViolencia,        
        RegistroViolencia,        
        Denuncia,
        module='health_violence', type_='model')

